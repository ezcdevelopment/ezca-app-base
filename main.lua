-----------------------------------------------------------------------------------------
-- main.lua
-----------------------------------------------------------------------------------------

-- Hide the status bar.
--display.setStatusBar( display.HiddenStatusBar )


-- -- Open external links in the browser and email in the mail app.
-- local function urlHandler(event)

	-- local url = event.url

	-- if( string.find( url, "http:" ) ~= nil or string.find( url, "mailto:" ) ~= nil ) then

		-- print("url: ".. url)
		-- system.openURL(url)

	-- end

	-- return true
-- end

-- -- Location of my website files.
-- local url = "http://dev.ezcustomapps.com/app/EC9CE5”

-- Options for the web view.
-- local options = { hasBackground=false, baseUrl=system.ResourceDirectory, urlRequest=urlHandler }


-- -- Open my website in a web view.
-- native.showWebPopup( url, options )



------------------------------------------------------------
-- BUILD ID : 1.5.201409 
------------------------------------------------------------

local globals = require( "scripts.globals" )
local file = require( "scripts.mod_file" )
local ezca = require( "scripts.mod_ezca" )

------------------------------------------------------------
-- LOCAL INITS
------------------------------------------------------------
local appCode = ezca.appCode
local appURL = "http://ezcustomapps.com/ezapp/"..appCode

local pushbots = require( "scripts.mod_pushbots" )
pushbots:init( ezca.pushBots )
pushbots.showStatus = false

local onlineGiving = "off"
local onlineGivingURL = ""
local onlineGivingText = ""
------------------------------------------------------------

--------------------------------------------------------
-- Color For Button
--------------------------------------------------------

-- The gradient used by the title bar
local titleGradient = {
	type = 'gradient',
	color1 = { 189/255, 203/255, 220/255, 255/255 }, 
	color2 = { 89/255, 116/255, 152/255, 255/255 },
	direction = "down"
}

--------------------------------------------------------
-- Create WebView
--------------------------------------------------------

if onlineGiving == "on" then
    
	local function webListener( event )
	    if event.url then
	        print( "You are visiting: " .. event.url )
	    end
	
	    if event.type then
	        print( "The event.type is " .. event.type ) -- print the type of request
	    end
	
	    if event.errorCode then
	        native.showAlert( "Error!", event.errorMessage, { "OK" } )
	    end
	end
    
	local webView = native.newWebView( display.contentWidth*0.5, display.contentHeight*0.5-60, display.contentWidth, display.contentHeight-125 )
	
	webView:request( appURL )
	webView:addEventListener( "urlRequest", webListener )
	
	local button = display.newRect( display.contentCenterX, 0, display.contentWidth, 120 )
		button:setFillColor( titleGradient )
		button.y = display.contentHeight - button.contentHeight * 0.5
		button:toFront()
	
	local buttonText = display.newText(onlineGivingText,0,0,native.systemFont,40)
		buttonText:setFillColor( 1,1,1)
		buttonText.x = display.contentWidth*0.5
		buttonText.y = button.y
		
	function button:touch( event )
		if event.phase == "began" then
			
		elseif event.phase == "ended" then
	        system.openURL( onlineGivingURL )
	
		end
			
		return true
	end
	button:addEventListener( "touch", button )
	
else 
	local webView = native.newWebView( display.contentWidth*0.5, display.contentHeight*0.5, display.contentWidth, display.contentHeight )
	
	local function listener(event)
		print(event.type)
		print(event.url)
		
		if string.find(event.url, "ezcustomapps") then 
		else
			system.openURL( event.url )
		end
	end
	
	webView:addEventListener( "urlRequest", listener )
	webView:request( appURL, system.ResourceDirectory )
end

-- webView.isVisible = false

-- Web views are not supported in the Simulator, so show a warning message if this opens in the Simulator.
if "simulator" == system.getInfo("environment") then
	msg = "Web views are not supported in the Simulator!"
	msg = display.newText( msg, 0, 0, native.systemFont, 14 )
	msg.x, msg.y = display.contentWidth*0.5, display.contentHeight*0.5
end

--------------------------------------------------------
-- Create Native Logout
--------------------------------------------------------

local function onKeyEvent( event )
   local phase = event.phase
   local keyName = event.keyName
   -- print( event.phase, event.keyName )

   if ( "back" == keyName and phase == "up" ) then
		local function onComplete( event )
			if "clicked" == event.action then
				local i = event.index
				if 1 == i then
					native.cancelAlert( alert )
				elseif 2 == i then
					native.requestExit()
				end
			end
		end
		local alert = native.showAlert( "EXIT", "ARE YOU SURE?", { "NO", "YES" }, onComplete )
		-- sceneGroup:insert(alert)
		return true
   end
   return false  --SEE NOTE BELOW
end

--add the key callback
Runtime:addEventListener( "key", onKeyEvent )

--------------------------------
-- Check Internet Connection
--------------------------------

local socket = require("socket")
            
local test = socket.tcp()
test:settimeout(1000)    -- Set timeout to 1 second
           
local testResult = test:connect("www.google.com", 80)        -- Note that the test does not work if we put http:// in front

if not(testResult == nil) then
    print("Internet access is available")
else
    print("Internet access is not available")
	
	-- remove the webView as with any other display object
	webView:removeSelf()
	webView = nil
	
	local background = display.newRect( 0, 0, 360, 570 )
	background.anchorX = 0
	background.anchorY = 0
	background:setFillColor(0,0,0)
	background:toFront()
	
	local function onComplete( event )
		if "clicked" == event.action then
			local i = event.index
			if 1 == i then
				native.cancelAlert( alert )
			elseif 2 == i then
				native.requestExit()
			end
		end
	end
	local alert = native.showAlert( "No Internet Connection", "Exit?", { "NO", "YES" }, onComplete )
	-- sceneGroup:insert(alert)
end
            
test:close()
test = nil


------------------------------------------------------------
-- Load Push Settings Function
------------------------------------------------------------
local deviceToken = false

-- Try loading saved deviceToken
deviceToken = file.read( "device_token" )


-- Called when a notification event has been received.
local function onNotification(event)
	if event.type == "remoteRegistration" then
		-- This device has just been registered for push notifications.
		-- Store the Registration ID that was assigned to this application.
		deviceToken = event.token

		--store the token
		file.write( "device_token", deviceToken )

		------------------------------------------------------------
		-- PUSHBOTS REGISTRATION
		------------------------------------------------------------
		pushbots:registerDevice( deviceToken, pushbots.NIL, function(e) 
			if not e.error then
				--native.showAlert( "Pushbots", e.response, { "OK" } )
				if e.code == 200 then
					pushbots:clearBadgeCount( deviceToken )
				end
			else
				native.showAlert( "Pushbots", e.error, { "OK" } )
			end
		end)
		------------------------------------------------------------

		-- Print the registration event to the log.
		print("### --- Registration Event ---")
		globals.printTable(event)

	else
		-- A push notification has just been received. Print it to the log.
		print("### --- Notification Event ---")
		globals.printTable(event)

		--get token
		deviceToken = file.read( "device_token" )

		-- Reset badge count to 0
		pushbots:clearBadgeCount( deviceToken )

		--mark opened
		pushbots:pushOpened()
	end
end

-- Set up a notification listener.
Runtime:addEventListener("notification", onNotification)

Runtime:addEventListener( "system", function (e) 
	if "applicationResume" == e.type then
		pushbots:clearBadgeCount( deviceToken )
	end
end)
-- Print this app's launch arguments to the log.
-- This allows you to view what these arguments provide
-- when this app is started by tapping a notification.
local launchArgs = ...

print("### --- Launch Arguments ---")
globals.printTable(launchArgs)

-- Reset badge count to 0

if deviceToken then
	pushbots:clearBadgeCount( deviceToken )
end